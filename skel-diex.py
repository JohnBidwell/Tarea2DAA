from skimage import io
from skimage.color import rgb2gray
from skimage.filters.rank import entropy, gradient
from skimage.morphology import disk
import numpy as np
import matplotlib.pyplot as plt
import sys


def get(y, x, t):
    m = len(t) #rows
    n = len(t[0]) #cols
    if x < 0 or x >= n: return sys.maxsize
    if y < 0 or y >= m: return sys.maxsize
    return t[y][x]
    
## Transform the image to gray
## Calculate the entropy or gradient
## Get the path with less energy
## Return the energy as an integer, and the 
## path with less energy as a list of (x,y)
def energy(img):
    # filas = m , columnas = n 
    m, n = img.shape
    print(m, 'm', '', n,'n')
    ## ESTO ES LO QUE DEBE IMPLEMENTAR
    ## ESTE SIMPLEMENTE ELIMINA LA PRIMERA COLUMNA
    ans = [0 for j in range(m)]
    for x in range(1, m):
        e=0
        for y in range(n):
            if y==0:
                e=min(img[x-1][y], img[x-1][y+1])
            elif y<n-2:
                e=min(img[x-1][y], img[x-1][y+1], img[x-1][y-1])
            else:
                e=min(img[x-1][y], img[x-1][y-1])
            img[x][y]=img[x][y] + e
            
    for x in range(m-1, -1, -1):
        if x==m-1:
            minimo=img[x][0] ##arbitrario, mejorar
            for y in range(n):
                if minimo>img[x][y]:
                    minimo=img[x][y]
                    pos=y
                    ans[x]=(x, pos)
                    e=img[x][pos]
                    print(img[x][pos], 'e')
                    print(pos, 'pos')
        else:
            if pos<n-2:
                if img[x][pos-1]<img[x][pos] and img[x][pos-1]<img[x][pos+1]:
                    pos-=1
                    ans[x]=(x, pos)
                elif img[x][pos]<img[x][pos-1] and img[x][pos]<img[x][pos+1]:
                    ans[x]=(x,pos)
                ##elif img[x][pos+1]<img[x][pos-1] and img[x][pos+1]<img[x][pos]:
                else:
                    pos+=1
                    ans[x]=(x, pos)
            else:
                if img[x][pos-1]<img[x][pos] and img[x][pos-1]<img[x][pos+1]:
                    pos-=1
                    ans[x]=(x, pos)
                elif img[x][pos]<img[x][pos-1] and img[x][pos]<img[x][pos+1]:
                    ans[x]=(x,pos)
    return e, ans
## Remove one pixel per row... the one
## in the path min energy
def remove(image, pixels):
## Debe remover el camino con menor energia
    ans = image
    
    return ans


def togray(image):
    image_bw = rgb2gray(image)
    # using the entropy
    #image_e = entropy(image_bw,disk(1))
    #return energy(image_e)
    
    # using gradient
    image_g = gradient(image_bw,disk(1))
    return image_g


    
if __name__=='__main__':
    import sys
    
    image = io.imread('image.png')
    img_gray = togray(image)
    plt.figure()
    plt.imshow(image)
    plt.figure()
    plt.imshow(img_gray)
    plt.show()
    
    percent = 0.75
    
    m,n,_ = image.shape
    new_n = int(n * percent)
    
    img = image
    ims = []
    for i in range(n-new_n):
        print("Iteracion numero {}/{}".format(i+1, n-new_n))
        img_gray = togray(img)
        e, p = energy(img_gray)
        print(p, '- e:', e)
        img_new = remove(img, p)
        
        img = img_new
    
    plt.figure()
    plt.imshow(image) # imagen original
    plt.figure()
    plt.imshow(img) # imagen escalada
    plt.show()
