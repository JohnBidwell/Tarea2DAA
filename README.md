Tarea 2 de Diseño y análisis de algoritmos.

### Subir contenido

* Paso 1: Clonar repositorio
```
git clone git@gitlab.com:JohnBidwellB/Tarea2DAA.git
cd Tarea2DAA
```
* Paso 2: Añadir cambios (opcional)

  * Para ver si tenemos cambios:
```
git status
```
  * Añadir cambios
```
git add .
git commit -a -m "Mensaje que identifique los cambios hechos"
git push origin master
```

### Bajar cambios
```
git pull origin
```
